function switchLangID(langId) {
    var tra = {
        "1": [
            "You seem to be using an unsupported browser",
            "Get the most out of POKERSTARS STORE with a supported browser"
        ],
        "2": [
            "Il semblerait que vous utilisiez un navigateur non pris en charge.",
            "Profitez pleinement de POKERSTARS STORE avec un navigateur pris en charge."
        ],
        "3": [
            "Parece que usas un navegador no compatible",
            "Saca el máximo partido a la TIENDA DE POKERSTARS con un navegador compatible"
        ],
        "4": [
            "Sembra tu stia usando un browser non supportato",
            "Ottieni il massimo dal POKERSTARS STORE utilizzando un browser supportato"
        ],
        "5": [
            "Sie scheinen einen Browser zu nutzen, der nicht unterstützt wird",
            "Holen Sie mit einem unterstützten Browser das meiste aus dem POKERSTARS-SHOP heraus"
        ],
        "6": [
            "Wygląda na to, że używasz nieobsługiwanej przeglądarki.",
            "Wykorzystaj SKLEP POKERSTARS w pełni, otwierając go w",
        ],
        "7": [
            "Vypadá to, že používáte prohlížeč, který nepodporujeme",
            "S podporovaným prohlížečem budete moci využít OBCHOD POKERSTARS naplno."
        ],
        "8": [
            "Det ser ud til, at du bruger en browser, der ikke er understøttet",
            "Få mest muligt ud af POKERSTARS STORE med en understøttet browser"
        ],
        "10": [
            "Käytät selainta, jota ei ole tuettu",
            "Ota kaikki irti POKERSTARS-KAUPASTA tuetulla selaimella"
        ],
        "11": [
            "Úgy tűnik, nem támogatott böngészőt használsz.",
            "Hozd ki a legtöbbet a POKERSTARS ÁRUHÁZBÓL egy támogatott böngészővel.",
        ],
        "12": [
            "Der ser ut til at du bruker en nettleser som ikke støttes",
            "Få mest mulig ut av POKERSTARS STORE med en nettleser som støttes"
        ],
        "13": [
            "Parece estar a utilizar um browser que não é suportado.",
            "Aproveite a LOJA DA POKERSTARS ao máximo com um browser suportado."
        ],
        // "14": [
        // Slovakian
        // ],
        "15": [
            "Du verkar använda en webbläsare som inte stöds",
            "Få ut maximalt av POKERSTARS STORE med en webbläsare som stöds"
        ],
        "16": [
            "不支持当前所用浏览器",
            "请使用受支持的浏览器，尽享PokerStars扑克之星商店",
        ],
        "17": [
            "ご利用中のブラウザーはサポート対象外です。",
            "PokerStars ストアを最大限に活用するにはサポート対象のブラウザーをご利用ください。"
        ],
        "18": [
            "您似乎使用不支援的網路瀏覽器",
            "請使用支援的網路瀏覽器，盡享POKERSTARS撲克之星商店非凡體驗",
        ],
        // "19": , KOREAN
        "20": [
            "Изглежда използвате неподдържан браузър",
            "За да се възползвате от всички функции на МАГАЗИНА НА POKERSTARS, използвайте поддържан браузър"
        ],
        "21": [
            "Čini se da koristite preglednik koji nije podržan",
            "Uživajte u POKERSTARS TRGOVINI uz podržani preglednik"
        ],
        "22": [
            "Tundub, et kasutad veebibrauserit, mida me ei toeta.",
            "POKERSTARSI POOD töötab paremini, kui avad selle veebibrauseris, mida toetame."
        ],
        "23": [
            "Φαίνεται ότι χρησιμοποιείς πρόγραμμα περιήγησης που δεν υποστηρίζεται",
            "Επωφελήσου στο μέγιστο από το POKERSTARS STORE με ένα πρόγραμμα περιήγησης που υποστηρίζεται."
        ],
        "24": [
            "Šķiet, ka Jūs izmantojat neatbalstītu pārlūkprogrammu.",
            "Lai pilnvērtīgi izmantotu POKERSTARS VEIKALU, lietojiet atbalstītu pārlūkprogrammu."
        ],
        "25": [
            "Panašu, kad naudojate nepalaikomą naršyklę",
            "Išnaudokite visas POKERSTARS PARDUOTUVĖS galimybes naudodami palaikomą naršyklę"
        ],
        "26": [
            "Parece que você está usando um navegador não suportado",
            "Tire o máximo da LOJA POKERSTARS com um navegador suportado"
        ],
        "27": [
            "Se pare că utilizezi un browser nesuportat",
            "Obţine cât mai mult din MAGAZINUL POKERSTARS cu un browser suportat",
        ],
        "28": [
            "Вы используете неподдерживаемый веб-обозреватель.",
            "Для того чтобы пользоваться всеми функциями и возможностями МАГАЗИНА POKERSTARS, используйте поддерживаемый веб-обозреватель"
        ],
        "29": [
            "Zdi se, da uporabljaš nezdružljiv brskalnik.",
            "Zagotovi si združljiv brskalnik za nemoteno uporabo TRGOVINE POKERSTARS."
        ],
        "30": [
            "Al parecer estás usando un navegador no compatible",
            "Aprovecha al máximo la TIENDA DE POKERSTARS con un navegador compatible"
        ], 
        "31": [
            "Ви використовуєте браузер, що не підтримується.",
            "Щоб скористатися всіма функціями та можливостями МАГАЗИНУ POKERSTARS, використовуйте браузер, що підтримується"
        ]
    };

    var lang = tra[langId];


    document.querySelectorAll('.x-locales a').forEach(function(el){
        el.classList.remove('active');
    });

    document.querySelector('a[data-lang-id="' + langId + '"]').classList.add('active');

    document.querySelector('.unsupported-browser h1').textContent = lang[0];
    document.querySelector('.unsupported-browser p').textContent = lang[1];
}

window.onload = function() {
    document.querySelectorAll('.x-locales a').forEach(function(el){
        console.log(el);
        el.onclick = function() {
            var id = el.getAttribute('data-lang-id');
            switchLangID(id);
        }
    });
}